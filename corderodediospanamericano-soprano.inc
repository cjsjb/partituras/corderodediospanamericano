\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key e \minor

		R2*9  |
%% 10
		r4 r8 b  |
		e' 4 d' 8 fis'  |
		e' 2  |
		R2  |
		r4 r8 e'  |
%% 15
		b' 8 b' b' b'  |
		a' 4 g' 8 a'  |
		b' 8 b' 4. ~  |
		b' 4 b' 8 g'  |
		e' 2 ~  |
%% 20
		e' 4 d' 8 fis'  |
		e' 8 e' 4. ~  |
		e' 4 b' 8 g'  |
		e' 2 ~  |
		e' 4 d' 8 fis'  |
%% 25
		e' 8 e' 4. ~  |
		e' 4 r8 b  |
		e' 4 d' 8 fis'  |
		e' 2  |
		R2  |
%% 30
		r4 r8 e'  |
		b' 8 b' b' b'  |
		a' 4 g' 8 a'  |
		b' 8 b' 4. ~  |
		b' 4 b' 8 g'  |
%% 35
		e' 2 ~  |
		e' 4 d' 8 fis'  |
		e' 8 e' 4. ~  |
		e' 4 b' 8 g'  |
		e' 2 ~  |
%% 40
		e' 4 d' 8 fis'  |
		e' 8 e' 4. ~  |
		e' 4 r8 b  |
		e' 4 d' 8 fis'  |
		e' 2  |
%% 45
		R2  |
		r4 r8 e'  |
		b' 8 b' b' b'  |
		a' 4 g' 8 a'  |
		b' 8 b' 4. ~  |
%% 50
		b' 4 b' 8 ( g' )  |
		e' 2 ~  |
		e' 4 d' 8 ( fis' )  |
		e' 2 ~  |
		e' 4 b' 8 ( g' )  |
%% 55
		e' 2 ~  |
		e' 4 d' 8 ( fis' )  |
		e' 2 ~  |
		e' 2 ~  |
		e' 2  |
%% 60
		R2*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do, __
		ten pie -- dad __
		de no -- so -- tros, __
		ten pie -- dad __
		de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do, __
		ten pie -- dad __
		de no -- so -- tros, __
		ten pie -- dad __
		de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do, __
		da -- nos __
		la __ paz, __
		da -- nos __
		la __ paz. __
	}
>>
