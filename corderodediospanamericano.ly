% This LilyPond file was generated by Rosegarden 1.5.1
\include "nederlands.ly"
\version "2.10.0"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)
\header {
	title = "Cordero de Dios"
	subtitle = "Panamericano"
	tagline = "Coro Juvenil San Juan Bosco"
}
#(set-global-staff-size 20)
#(set-default-paper-size "letter")
\paper {
	#(define line-width (* 7 in))
	print-first-page-number = ##t
	ragged-bottom = ##t
	first-page-number = 1
}
global = {
	\time 2/4
}
globalTempo = {
	\tempo 4 = 93
}
\score {
	<<
		% force offset of colliding notes in chords:
		\override Score.NoteColumn #'force-hshift = #1.0

		\include "corderodediospanamericano-acordes.inc"
		\new StaffGroup <<
			\include "corderodediospanamericano-soprano.inc"
			\include "corderodediospanamericano-mezzo.inc"
			\include "corderodediospanamericano-contralto.inc"
			\include "corderodediospanamericano-tenor.inc"
		>>
		\include "corderodediospanamericano-zamponna.inc"

	>>

	\layout {
		\context {
			\RemoveEmptyStaffContext
		}
		\context { \ChordNames \consists Instrument_name_engraver }
	}
}
