\context Staff = "zamponna" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Zampoña"
	\set Staff.shortInstrumentName = "Z."
	\set Staff.midiInstrument = "Blown Bottle"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-zamponna" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
                \key e \minor

		e' 8 e' 16 fis' g' 8 g' 16 a'  |
		b' 8 b' 16 b' 8 b' 16 a' 8  |
		g' 8 g' 16 g' 8 g' 16 fis' 8  |
		e' 2  |
%% 5
		e' 16 d' e' fis' g' fis' g' a'  |
		b' 8 b' 16 b' 8 b' 16 a' 8  |
		g' 8 g' 16 g' 8 g' 16 fis' 8  |
		e' 4 d' 8 fis'  |
		e' 2  |
%% 10
		R2*47  |
		e' 8 e' 16 fis' g' 8 g' 16 a'  |
		b' 8 b' 16 b' 8 b' 16 a' 8  |
		g' 8 g' 16 g' 8 g' 16 fis' 8  |
%% 60
		e' 2  |
		R2  |
		\bar "|."
	}
>>
