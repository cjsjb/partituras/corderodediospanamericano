\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key e \minor

		R2*11  |
		r4 r8 b,  |
		e 4 d 8 fis  |
		e 4. e 8  |
		b 8 b b b  |
%% 10
		a 4 g 8 a  |
		b 8 b 4. ~  |
		b 4 r  |
		r4 b 8 g  |
		e 4 d 8 fis  |
%% 15
		e 8 e 4. ~  |
		e 4 r  |
		r4 b 8 g  |
		e 4 d 8 fis  |
		e 8 e 4. ~  |
%% 20
		e 4 r  |
		R2  |
		r4 r8 b,  |
		e 4 d 8 fis  |
		e 4. e 8  |
%% 25
		b 8 b b b  |
		a 4 g 8 a  |
		b 8 b 4. ~  |
		b 4 r  |
		r4 b 8 g  |
%% 30
		e 4 d 8 fis  |
		e 8 e 4. ~  |
		e 4 r  |
		r4 b 8 g  |
		e 4 d 8 fis  |
%% 35
		e 8 e 4. ~  |
		e 4 r  |
		R2  |
		r4 r8 b,  |
		e 4 d 8 fis  |
%% 40
		e 4. e 8  |
		b 8 b b b  |
		a 4 g 8 a  |
		b 8 b 4. ~  |
		b 4 r  |
%% 45
		r4 b 8 ( g )  |
		e 4 d 8 ( fis )  |
		e 2 ~  |
		e 4 r  |
		r4 b 8 ( g )  |
%% 50
		e 4 d 8 ( fis )  |
		e 2 ~  |
		e 2 ~  |
		e 2  |
		R2*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do, __
		ten pie -- dad
		de no -- so -- tros, __
		ten pie -- dad
		de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do, __
		ten pie -- dad
		de no -- so -- tros, __
		ten pie -- dad
		de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do, __
		da -- nos
		la __ paz, __
		da -- nos
		la __ paz. __
	}
>>
