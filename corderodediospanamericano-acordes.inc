\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		s2 g2 d2 e2:m
		e2:m g2 d2 e4:m d4
		e2:m e2:m

		% cordero de dios...
		e4:m d4 e2:m e4:m d4 e2:m
		g2 d2 g2 g2
		e2:m e4:m d4 e2:m e2:m
		e2:m e4:m d4 e2:m e2:m

		% cordero de dios...
		e4:m d4 e2:m e4:m d4 e2:m
		g2 d2 g2 g2
		e2:m e4:m d4 e2:m e2:m
		e2:m e4:m d4 e2:m e2:m

		% cordero de dios...
		e4:m d4 e2:m e4:m d4 e2:m
		g2 d2 g2 g2
		e2:m e4:m d4 e2:m e2:m
		e2:m e4:m d4 e2:m
		e2:m e2:m e2:m
	}
