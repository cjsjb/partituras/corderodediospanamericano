\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key e \minor

		R2*11  |
		r4 r8 b  |
		e' 4 d' 8 d'  |
		b 4. b 8  |
		g' 8 g' g' g'  |
%% 10
		fis' 4 e' 8 fis'  |
		g' 8 g' 4. ~  |
		g' 4 b' 8 g'  |
		e' 2 ~  |
		e' 4 d' 8 d'  |
%% 15
		b 8 b 4. ~  |
		b 4 b' 8 g'  |
		e' 2 ~  |
		e' 4 d' 8 d'  |
		b 8 b 4. ~  |
%% 20
		b 4 r  |
		R2  |
		r4 r8 b  |
		e' 4 d' 8 d'  |
		b 4. e' 8  |
%% 25
		g' 8 g' g' g'  |
		fis' 4 e' 8 fis'  |
		g' 8 g' 4. ~  |
		g' 4 b' 8 g'  |
		e' 2 ~  |
%% 30
		e' 4 d' 8 d'  |
		b 8 b 4. ~  |
		b 4 b' 8 g'  |
		e' 2 ~  |
		e' 4 d' 8 d'  |
%% 35
		b 8 b 4. ~  |
		b 4 r  |
		R2  |
		r4 r8 b  |
		e' 4 d' 8 d'  |
%% 40
		b 4. e' 8  |
		g' 8 g' g' g'  |
		fis' 4 e' 8 fis'  |
		g' 8 g' 4. ~  |
		g' 4 b' 8 ( g' )  |
%% 45
		e' 2 ~  |
		e' 4 d' 8 ~ d'  |
		b 2 ~  |
		b 4 b' 8 ( g' )  |
		e' 2 ~  |
%% 50
		e' 4 d' 8 ~ d'  |
		b 2 ~  |
		b 2 ~  |
		b 2  |
		R2*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do, __
		ten pie -- dad __
		de no -- so -- tros, __
		ten pie -- dad __
		de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do, __
		ten pie -- dad __
		de no -- so -- tros, __
		ten pie -- dad __
		de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do, __
		da -- nos __
		la __ paz, __
		da -- nos __
		la __ paz. __
	}
>>
